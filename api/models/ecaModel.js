//Database section - MongoDB
//Created by Lucas Quintiliano
//All Right Reserved - 2018

'use strict';

// MongoDB lib assignments
const mongoClient = require('mongodb').MongoClient;
var ObjectID = require('mongodb').ObjectID;

//Filesystem vars
const fs = require('fs');
const path = require("path");

//DB Local Objects
var mongoDbObj;
var ecaDB;

//DB vars
var schemaName, dbUrl, portNo;
const queryQuantityLimit = 100;

//Assets constrains
var maxCaregivers, maxRoomsPerFloor, maxFloors;

function fullURL(){ return "mongodb://" +dbUrl +":" +portNo +"/" +schemaName;};

//ECA DB Startup and Management Section
async function startEcaDB(newUrl, newPort, newSchemaName){
	portNo = newPort, schemaName = newSchemaName, dbUrl = newUrl;
	await start();
	await bootDB();
}

async function start(dbURL, dbPort, dbSchema){
	if(isDatabaseUp()) return;
	try {
		mongoDbObj = await mongoClient.connect(fullURL(dbURL, dbPort, dbSchema));
		ecaDB = mongoDbObj.db(dbSchema);
	} catch (err) {
		console.log(err.stack);
	}
}

function shutdownDatabase(){
	if(isDatabaseUp()){
		mongoDbObj.close();
		ecaDB = null;
		mongoDbObj = null;
	}
}

function isDatabaseUp(){
	if(mongoDbObj){
		return mongoDbObj.isConnected();
	}
	return false;
}

async function databaseOK(){
	if(!isDatabaseUp()){
		await start("localhost", "27017", schemaName);
		return isDatabaseUp();
	}
	return true;
}

//ECA DB Boot Section
async function bootDB(){
	//creates DB with locked settings (if needed)
	await loadAndComputeAssets();
}

//ECA Database generic instructions' section
//insert data into ECA's DB
async function insertData(collectionName, dataObject){
	if(await databaseOK()) return await ecaDB.collection(collectionName).insertOne(dataObject);
	return null;
}

//query assignment data into ECA's DB
async function queryItemWithOneConditionalTwoMatchesSorted(tableName, greaterFieldName, greaterFielValue, condition1Name, condition1Value, condition2Name, condition2Value, sortColumnName, order, amountToQuery){
	if(await databaseOK()){
		var orderCode = -1;
		if(order == 'asc') orderCode = 1;
		
		var document = await ecaDB.collection(tableName).find({
			$and: [
				{ $and: [
					{ [condition1Name]: condition1Value},
					{ [condition2Name]: condition2Value }]},
				{ [greaterFieldName]: { $gt: greaterFielValue }}]}).sort({ [sortColumnName]: orderCode }).limit(amountToQuery).toArray();
		return document;
	}
	return null;
}

//update data into ECA's DB
async function updateOneDocumentWithID(collectionName, dataObjectToUpdate){
	if(await databaseOK()) return ecaDB.collection(collectionName).updateOne({ _id: dataObjectToUpdate._id }, {$set: dataObjectToUpdate});
	return null;
}

//remove data from ECA's DB
async function removeOneDocumentFromCollection(collectionName, objectToRemove){
	if(await databaseOK()) return ecaDB.collection(collectionName).deleteOne(objectToRemove);
}

//ECA first Informations inserting section
async function loadAndComputeAssets(){
	//ROOMS Section
	//load rooms from rooms JSON
    let roomsDataRaw = fs.readFileSync(path.resolve(__dirname, './assets/rooms.json'));  
	let roomsJSON = JSON.parse(roomsDataRaw); 
	maxRoomsPerFloor = parseInt(roomsJSON.maxRoomsPerFloor), maxFloors = parseInt(roomsJSON.maxFloors);
	// console.log("Rooms JSON: " + JSON.stringify(roomsJSON));  

	//Load all rooms DB Info
	var roomsOnDB = await getRoomsFromDB(null);

	//Compare roomsJSON with DB, update if necessary
	if(!roomsOnDB || roomsOnDB.length != (maxRoomsPerFloor * maxFloors)){
		//insert JSON rooms (or generic) on DB
		await insertRooms(roomsOnDB.length, (maxRoomsPerFloor * maxFloors), roomsJSON);
	}

	//CAREGIVERS Section
    //load caregivers JSON
    let caregiversDataRaw = fs.readFileSync(path.resolve(__dirname, './assets/caregivers.json'));  
	let caregiversJSON = JSON.parse(caregiversDataRaw);
	maxCaregivers = parseInt(caregiversJSON.caregiversCount);
	// console.log("Caregivers JSON: " + JSON.stringify(caregiversJSON));

	//Load all caregivers DB Info
	var caregiversOnDB = await getCaregiversFromDB(null);
	
	//Compare caregiversJSON with DB, update if necessary
	if(!caregiversOnDB || caregiversOnDB.length != maxCaregivers){
		//insert JSON rooms (or generic) on DB
		await insertCaregivers(caregiversOnDB.length, maxCaregivers, caregiversJSON);
	}
}

//ECA MongoDB Queries
//GET info of MongoDB
async function getRoomsFromDB(count){
	//get rooms from DB (all if count is not specified)
	if(await databaseOK()){
		var amountToQuery = count;
		if(!count || count>maxRoomsPerFloor){
			amountToQuery = parseInt(maxRoomsPerFloor);
		}
		
		var document = await ecaDB.collection("room").find({}).sort({ ["roomNo"]: 1 }).limit(amountToQuery).toArray();
		return document;
	}
	return null;
}

async function getRoomByNumber(roomNumber){
	if(await databaseOK()){
		var document = await ecaDB.collection("room").find({ ["roomNo"]: ""+roomNumber }).limit(1).toArray();
		return document[0];
	}
	return null;
}

async function getAllCaregivers(){
	return await getCaregiversFromDB(maxCaregivers);
}

async function getCaregiverByNumber(caregiverNumber){
	if(await databaseOK()){
		var document = await ecaDB.collection("caregiver").find({ ["caregiverNo"]: ""+caregiverNumber }).limit(1).toArray();
		return document[0];
	}
	return null;
}

async function getCaregiversFromDB(count){
    //get caregivers from DB (all if count is not specified)
	if(await databaseOK()){
		var amountToQuery = count;
		if(!count || count>maxCaregivers){
			amountToQuery = parseInt(maxCaregivers);
		}
		
		var document = await ecaDB.collection("caregiver").find({}).sort({ ["caregiverNo"]: 1 }).limit(amountToQuery).toArray();
		return document;
	}
	return null;
}

async function getAssignmentByInfo(caregiverNo, roomNo, datetime){
	var caregiverObjDB;
	if(caregiverNo > 0 && caregiverNo <= maxCaregivers){
		caregiverObjDB = await getCaregiverByNumber(caregiverNo);
	}

	var roomObjDB;
	if(roomNo > 0 && roomNo <= maxRoomsPerFloor * maxFloors){
		roomObjDB = await getRoomByNumber(roomNo);
	}

	if(caregiverObjDB && roomObjDB){
		return await getAssignmentWithObjsDB(caregiverObjDB, roomObjDB, datetime);
	}

	return null;
}

async function getAssignmentWithObjsDB(caregiverObjDB, roomObjDB, datetime){
	if(await databaseOK()){
		var filter = {"caregiverID": caregiverObjDB._id, "roomID": roomObjDB._id, "datetime": datetime};
		var document = await ecaDB.collection("assignment").find(filter).limit(1).toArray();
		return document;
	}
	return null;
}

async function getAssignmentsByPage(count, page){ //all inclusive
	if(await databaseOK()){
		var filter = { $and: [
			{ "id": { $lte: ((count * page) - count) }},
			{ "id": { $gte: (count * page) }}
		]};
		var documentCount = count + 1;
		var document = await ecaDB.collection("assignment").find(filter).limit(documentCount).toArray();
		return document;
	}
	return null;
}

async function getAssignmentsByDatetime(datetime){ //all inclusive
	if(await databaseOK()){
		var filter = {"time": datetime};
		var document = await ecaDB.collection("assignment").find(filter).limit(1).toArray();
		return document[0];
	}
	return null;
}

//INSERT info into MongoDB
//Attention: "from" incluse and "to" exclusive on ALL methods
async function insertRooms(from, to, roomsJSON){
    //insert rooms that are missing into the DB
	for(var i=from; i<to; i++){
		// var newRoomNo = "-1", newFloorNo = "-1", genericRoom = { "roomNo": newRoomNo , "florrNo": newFloorNo}; //TODO implement generic room when not on JSON file
		await insertData("room", { "roomNo": ""+roomsJSON.rooms[i].roomNo, "floorNo": ""+roomsJSON.rooms[i].floor }); //TODO need to make maxFloors dynamically changeable
	}
}

async function insertCaregivers(from, to, caregiversJSON){
    //insert caregivers that are missing into the DB
	for(var i=from; i<to; i++){
		await insertData("caregiver", { "name": ""+caregiversJSON.caregivers[i].name, "caregiverNo": ""+caregiversJSON.caregivers[i].caregiverNo, "photobase64": ""+caregiversJSON.caregivers[i].photobase64 }); //TODO implement generic caregiver when not on JSON file
	}
}

async function insertAssignment(caregiverNo, roomNo, datetime, isFilledByAI){
	var caregiverObjDB;
	if(caregiverNo > 0 && caregiverNo <= maxCaregivers){
		caregiverObjDB = await getCaregiverByNumber(caregiverNo);
	}

	var roomObjDB;
	if(roomNo > 0 && roomNo <= maxRoomsPerFloor * maxFloors){
		roomObjDB = await getRoomByNumber(roomNo);
	}

	if(caregiverObjDB && roomObjDB){
		//TODO_REAL : check if assignment exists for datetime specified
		var assignmentObjDB = await getAssignmentsByDatetime(datetime);
		if(assignmentObjDB){
			var assignmentRoomID = assignmentObjDB.roomID.toHexString();
			var roomObjID = roomObjDB._id.toHexString();

			var assignmentCaregiverID = assignmentObjDB.caregiverID.toHexString();
			var caregiverObjID = caregiverObjDB._id.toHexString();

			if(assignmentRoomID !== roomObjID || assignmentCaregiverID !== caregiverObjID){ //needs update?
				//yes, then update it
				assignmentObjDB.roomID = roomObjDB._id;
				assignmentObjDB.caregiverID = caregiverObjDB._id;
				updateOneDocumentWithID("assignment", assignmentObjDB);
			}
		}else{ //it not, create it
			var newAssignmentObj = { "time": datetime, "aifill": isFilledByAI ? "0" : "1", "roomID": roomObjDB._id, "caregiverID": caregiverObjDB._id};
			await insertData("assignment", newAssignmentObj);
		}
	}
}

//REMOVE info into MongoDB
async function removeAssignment(datetime){
	var assignmentObjDB = await getAssignmentsByDatetime(datetime);
	if(assignmentObjDB){
		await removeOneDocumentFromCollection("assignment", assignmentObjDB);
	}
}

module.exports = {
	startEcaDB,
	getAllCaregivers, getCaregiverByNumber,
	getAssignmentsByPage, getAssignmentsByDatetime,
	insertAssignment, removeAssignment
};