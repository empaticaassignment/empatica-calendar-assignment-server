//Controller section - ECA's Controller of ECA data between API and Model (JSON and/or MongoDB)
//Created by Lucas Quintiliano
//All Right Reserved - 2018

'use strict';

var ecaModel = require('../models/ecaModel');

const maxAssignmentsPerRequest = 100;

//constants, setup and start ECA Mongodb's DB
const dbServerAddr = "127.0.0.1";
const dbPort = 27017;
const dbSchameName = "ECA";
ecaModel.startEcaDB(dbServerAddr, dbPort, dbSchameName);

//Caregivers
async function list_all_caregivers(req, res) {
  var allCaregivers = await ecaModel.getAllCaregivers();
  res.json(allCaregivers);
};

async function read_caregiver(req, res) {
  var singleCaregiver = await ecaModel.getCaregiverByNumber(req.params.caregiverId);
  res.json(singleCaregiver);
};

//Assignments
async function list_all_assignments(req, res) {
  var allAssignmentsLimited = await ecaModel.getAssignmentsByPage(maxAssignmentsPerRequest, 1);
  res.json(allAssignmentsLimited);
};

async function read_assignment(req, res) { //TODO_REAL
  var dateTime = req.params.datetime;
  var assignmentFromDB = await ecaModel.getAssignmentsByDatetime(dateTime);
  res.json(assignmentFromDB);
};

async function update_assignment(req, res) {
  for(var dataToInsert of req.body.assignments){
    await ecaModel.insertAssignment(dataToInsert.caregiverNo, dataToInsert.roomNo, dataToInsert.datetime, dataToInsert.aifill);
  }
  res.send(200);
};

async function delete_assignment(req, res) {
  var dateTime = req.params.datetime;
  await ecaModel.removeAssignment(dateTime);
  res.send(200);
};

module.exports = {
  list_all_caregivers, read_caregiver,
  list_all_assignments, read_assignment, update_assignment, delete_assignment
}