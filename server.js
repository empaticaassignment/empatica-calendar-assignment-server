//ECA (Empatica Calendar Assignment) - Web Server Side App - NodeJS
//MAIN INDEX FILE
//Created by Lucas Quintiliano
//All Right Reserved - 2018

'use strict';

var bodyParser = require('body-parser');
var express = require('express');

const serverAddr = "127.0.0.1";
const portNo = 3001;

var app = express();
var port = process.env.PORT || portNo;
app.use(bodyParser.json());

// var ecaRoutes = require('./api/routes/ecaRoutes');
// app.use('/api', ecaRoutes());

app.listen(port);
console.log('Empatica Calendar Assignment Server side App started successfully on port ' + port);

app.get('/', function (req, res) {
  res.send('Empatica Calendar Assignment Server side App');
});

var ecaController = require('./api/controllers/ecaController');

app.get('/api/caregivers/', ecaController.list_all_caregivers);
app.get('/api/caregivers/:caregiverId', ecaController.read_caregiver);

app.get('/api/assignments/', ecaController.list_all_assignments);
app.get('/api/assignments/:datetime', ecaController.read_assignment);
app.put('/api/assignments/', ecaController.update_assignment);
app.delete('/api/assignments/:datetime', ecaController.delete_assignment);

module.exports = {
  
}